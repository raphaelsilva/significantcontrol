using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Company.CompanyHouse
{
    public static class ch
    {
        private const string BASE_URL = "https://api.company-information.service.gov.uk";
        private const string DATALAKE_PATH = "c-house-lake/landing/persons-with_significant-control/2021/10/25/companies.json";
        private const int MAX_THREADS = 150;
        private static HttpClient _client = null;

        [FunctionName("significantcontrol")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = null)] HttpRequest req,
            [Blob(DATALAKE_PATH, FileAccess.Write, Connection = "CHouseStorage")] TextWriter tw,
            ILogger log)
        {

            _client = new HttpClient();
            var semaphore = new SemaphoreSlim(MAX_THREADS, MAX_THREADS);

            _client.DefaultRequestHeaders.Add("Authorization",  Environment.GetEnvironmentVariable("AUTH_HEADER"));

            var param = await new StreamReader(req.Body).ReadToEndAsync();

            if (string.IsNullOrEmpty(param))
            {
                return new BadRequestResult();
            }

            var request = JsonSerializer.Deserialize<SignificantControlRequest>(param);
            var companyIds = request.companyIds;

            var urls = companyIds.Select(e => EnsembleUri(e));
            var tasks = urls.Select(c => GetDataAsync(c, semaphore, log));

            var allResponses = await Task.WhenAll(tasks);
            var data = DeserializeResponse(allResponses.ToList(), log);

            // writes data into the Blob container
            tw.Write(JsonSerializer.Serialize(data));

            return new OkObjectResult(data);
        }

        private static List<Root> DeserializeResponse(List<string> responses, ILogger log)
        {
            var res = new List<Root>();

            foreach (var item in responses)
            {
                try
                {
                    res.Add(JsonSerializer.Deserialize<Root>(item));
                }
                catch (Exception ex)
                {
                    log.LogInformation($"Deserialization exception error: {ex.Message}");
                    continue;
                }
            }

            return res;
        }

        private static Uri EnsembleUri(string companyId)
        {
            return new Uri($"{BASE_URL}/company/{companyId}/persons-with-significant-control");
        }

        private static async Task<string> GetDataAsync(Uri uri, SemaphoreSlim semaphore, ILogger log)
        {
            try
            {
                await semaphore.WaitAsync();
                
                var response = await _client.GetAsync(uri.ToString());
                return await response.Content.ReadAsStringAsync();

            }
            catch (HttpRequestException ex)
            {
                log.LogInformation(ex.Message);
                throw;
            }
            finally
            {
                semaphore.Release();
            }
        }
    }


    // Models
    public class SignificantControlRequest
    {
        [JsonPropertyName("body")]
        public List<string> companyIds { get; set; }
    }

    public class Links
    {
        public string self { get; set; }
    }

    public class DateOfBirth
    {
        public int month { get; set; }
        public int year { get; set; }
    }

    public class Address
    {
        public string address_line_1 { get; set; }
        public string postal_code { get; set; }
        public string country { get; set; }
        public string locality { get; set; }
        public string premises { get; set; }
    }

    public class NameElements
    {
        public string surname { get; set; }
        public string title { get; set; }
        public string forename { get; set; }
    }

    public class Item
    {
        public string kind { get; set; }
        public DateOfBirth date_of_birth { get; set; }
        public Address address { get; set; }
        public string nationality { get; set; }
        public Links links { get; set; }
        public List<string> natures_of_control { get; set; }
        public string notified_on { get; set; }
        public string etag { get; set; }
        public NameElements name_elements { get; set; }
        public string country_of_residence { get; set; }
        public string name { get; set; }
    }

    public class Root
    {
        public int ceased_count { get; set; }
        public Links links { get; set; }
        public int total_results { get; set; }
        public int start_index { get; set; }
        public int items_per_page { get; set; }
        public int active_count { get; set; }
        public List<Item> items { get; set; }
    }

}
